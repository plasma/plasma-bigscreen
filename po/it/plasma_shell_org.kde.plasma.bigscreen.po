# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-bigscreen package.
# SPDX-FileCopyrightText: 2021, 2024 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-bigscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-10-17 00:41+0000\n"
"PO-Revision-Date: 2024-10-19 17:31+0200\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.08.2\n"

#: contents/configuration/ConfigurationContainmentAppearance.qml:49
msgid "Layout cannot be changed while widgets are locked"
msgstr "La disposizione non può essere modificata se gli oggetti sono bloccati"

#: contents/configuration/ConfigurationContainmentAppearance.qml:60
msgid "Wallpaper Type:"
msgstr "Tipo di sfondo:"

#: contents/configuration/ContainmentConfiguration.qml:37
msgid "Wallpaper"
msgstr "Sfondo"

#: contents/configuration/ContainmentConfiguration.qml:130
msgid "Slideshow"
msgstr "Presentazione"

#: contents/lockscreen/PasswordDialog.qml:46
msgid "Password"
msgstr "Password"

#: contents/lockscreen/PasswordDialog.qml:76
msgid "Unlock Screen"
msgstr "Sblocca schermo"
